﻿using WebBanHang_2180604857.Models;
namespace WebBanHang_2180604857.ViewModels
{
    public class HomeProductDetailViewModel
    {
        public Product products {  get; set; }
        public List<ImageSp> anhSps { get; set;}
    }
}
