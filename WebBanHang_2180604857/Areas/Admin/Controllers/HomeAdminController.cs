﻿using WebBanHang_2180604857.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using X.PagedList;
using WebBanHang_2180604857.DataAccess;
using WebBanHang_2180604857.Controllers;
namespace WebBanHang_2180604857.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin")]
    [Route("admin/homeadmin")]
    public class HomeAdminController : Controller
    {
        private readonly ILogger<HomeAdminController> _logger;
        private readonly ApplicationDbContext _db;
        public HomeAdminController(ApplicationDbContext db, ILogger<HomeAdminController> logger)
        {
            _db = db;
            _logger = logger;
        }
        [Route("")]
        [Route("index")]
        public IActionResult Index()
        {
            return View();
        }

        // Action cho signout
        [HttpGet]
        [Route("Signout")]
        public IActionResult Signout()
        {
            // Xóa Session hoặc đăng xuất người dùng ở đây
            HttpContext.Session.Clear();
            // Sau đó, chuyển hướng đến trang đăng nhập
            return RedirectToAction("Login", "Account", new { area = "" });
        }
        //Danh sách sản phẩm
        [Route("danhmucsanpham")]
        public IActionResult DanhMucSanPham(int? page)
        {
            int pageSize = 20;
            int pageNumber = page == null || page < 0 ? 1 : page.Value;
            var lstSanPham = _db.Products.AsNoTracking().OrderBy(x => x.Name);
            PagedList<Product> lst = new PagedList<Product>(lstSanPham, pageNumber, pageSize);
            return View(lst);
        }
        
        //Thêm mới 1 sản phẩm
        [Route("ThemSanPham")]
        [HttpGet]
        public IActionResult ThemSanPham()
        {
            ViewBag.Id = new SelectList(_db.Categories.ToList(), "Id", "Name");

            return View();
        }
        [Route("ThemSanPham")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ThemSanPham(Product sanPham)
        {
            if (ModelState.IsValid)
            {
                _db.Products.Add(sanPham);
                _db.SaveChanges();
                return RedirectToAction("DanhMucSanPham");
            }
            return View(sanPham);
        }
        //Chỉnh sửa sản phẩm
        [Route("SuaSanPham")]
        [HttpGet]
        public IActionResult SuaSanPham(int maSanPham)
        {
            ViewBag.Id = new SelectList(_db.Categories.ToList(), "Id", "Name");


            var sanPham = _db.Products.Find(maSanPham);
            return View(sanPham);
        }
        [Route("SuaSanPham")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SuaSanPham(Product sanPham)
        {
            if (ModelState.IsValid)
            {
                _db.Update(sanPham);
                _db.SaveChanges();
                return RedirectToAction("DanhMucSanPham", "HomeAdmin");
            }
            return View(sanPham);
        }
        // Xóa Sản Phẩm - HttpGet
        [Route("XoaSanPham")]
        [HttpGet]
        public IActionResult XoaSanPham(int maSanPham)
        {
            TempData["Message"] = "";

            // Xóa hình ảnh của sản phẩm (nếu có)
            var anhSanPham = _db.imageSps.Where(x => x.Id == maSanPham);
            if (anhSanPham.Any())
                _db.RemoveRange(anhSanPham);

            // Xóa sản phẩm
            var sanPham = _db.Products.Find(maSanPham);
            if (sanPham != null)
            {
                _db.Products.Remove(sanPham);
                _db.SaveChanges();
                TempData["Message"] = "Đã xóa sản phẩm";
            }
            else
            {
                TempData["Message"] = "Không tìm thấy sản phẩm để xóa";
            }

            return RedirectToAction("DanhMucSanPham", "HomeAdmin");
        }


        //Xóa Sản Phẩm - HttpPost
        [Route("XoaSanPham")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult XoaSanPham(Product sanPham)
        {
            if (ModelState.IsValid)
            {
                _db.Remove(sanPham);
                _db.SaveChanges();
                return RedirectToAction("DanhMucSanPham", "HomeAdmin");
            }
            return View(sanPham);
        }
//Loại Sản Phẩm 
        [Route("loaisanpham")]
        public IActionResult LoaiSanPham(int? page)
        {
            int pageSize = 20;
            int pageNumber = page == null || page < 0 ? 1 : page.Value;
            var lstLoaiSp = _db.Categories.AsNoTracking().OrderBy(x => x.Name);
            PagedList<Category> lst = new PagedList<Category>(lstLoaiSp, pageNumber, pageSize);
            return View(lst);
        }
        //Thêm mới 1 loại sản phẩm mới
        [Route("ThemLoaiSp")]
        [HttpGet]
        public IActionResult ThemLoaiSp()
        {
            var listLoaiSp = _db.Categories.ToList();
            if (listLoaiSp != null && listLoaiSp.Any())
            {
                ViewBag.Id = new SelectList(listLoaiSp, "Id", "Name");
            }
            else
            {
                TempData["ErrorMessage"] = "Không có loại sản phẩm nào có sẵn để chọn.";
            }
            return View();
        }
        [Route("ThemLoaiSp")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ThemLoaiSp(Category loaiSp)
        {
            if (ModelState.IsValid)
            {
                _db.Categories.Add(loaiSp);
                _db.SaveChanges();
                return RedirectToAction("LoaiSanPham");
            }
            return View(loaiSp);
        }
        //Chỉnh sửa sản phẩm
        [Route("SuaLoaiSp")]
        [HttpGet]
        public IActionResult SuaLoaiSp(int maloaiSp)
        {
            ViewBag.Id = new SelectList(_db.Categories.ToList(), "Id", "Name");

            var loaiSp = _db.Categories.Find(maloaiSp);
            return View(loaiSp);
        }
        [Route("SuaLoaiSp")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SuaLoaiSp(Category maloaiSp)
        {
            if (ModelState.IsValid)
            {
                _db.Update(maloaiSp);
                _db.SaveChanges();
                return RedirectToAction("LoaiSanPham", "HomeAdmin");
            }
            return View(maloaiSp);
        }
        // Xóa Sản Phẩm - HttpGet
        [Route("XoaLoaiSanPham")]
        [HttpGet]
        public IActionResult XoaLoaiSanPham(int maloaiSp)
        {
            TempData["Message"] = "";

            // Xóa loại sản phẩm
            var loaiSp = _db.Categories.Find(maloaiSp);
            if (maloaiSp != null)
            {
                _db.Categories.Remove(loaiSp);
                _db.SaveChanges();
                TempData["Message"] = "Đã xóa loại sản phẩm";
            }
            else
            {
                TempData["Message"] = "Không tìm thấy sản phẩm để xóa";
            }

            return RedirectToAction("LoaiSanPham", "HomeAdmin");
        }

        //Xóa Sản Phẩm - HttpPost
        [Route("XoaLoaiSanPham")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult XoaLoaiSanPham(Category maloaiSp)
        {
            if (ModelState.IsValid)
            {
                _db.Remove(maloaiSp);
                _db.SaveChanges();
                return RedirectToAction("LoaiSanPham", "HomeAdmin");
            }
            return View(maloaiSp);
        }
    }
}
