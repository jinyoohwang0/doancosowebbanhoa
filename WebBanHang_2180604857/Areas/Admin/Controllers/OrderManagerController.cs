﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebBanHang_2180604857.DataAccess;
using WebBanHang_2180604857.Models;
using WebBanHang_2180604857.Repositories;

namespace WebBanHang_2180604857.Areas.Admin.Controllers
{
    [Area("Admin")]

    public class OrderManagerController : Controller
    {

        private readonly ApplicationDbContext _context;

        private readonly IProductRepository _productReponsitory;

        private readonly UserManager<ApplicationUser> _userManager;
        public OrderManagerController(IProductRepository productReponsitory, UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _productReponsitory = productReponsitory;
            _userManager = userManager;
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            var orders = await _context.Orders.AsNoTracking()
                                  .Include(o => o.User) // Include thông tin ApplicationUser
                                  .ToListAsync();
            return View(orders);
        }


        public async Task<IActionResult> Details(int Id)
        {
            var orderDetails = await _context.OrderDetails
                .Include(od => od.Order)
                    .ThenInclude(order => order.User)
                .Include(od => od.Product)
                .Where(od => od.OrderId == Id)
                .ToListAsync();

            if (orderDetails == null || orderDetails.Count == 0)
            {
                return NotFound("Không tìm thấy chi tiết đơn hàng.");
            }

            var order = orderDetails.FirstOrDefault()?.Order;
            if (order == null)
            {
                return NotFound("Không tìm thấy thông tin đơn hàng.");
            }

            ViewBag.OrderId = Id;
            ViewBag.Order = order;
            ViewBag.ProductNames = orderDetails.Select(od => od.Product.Name).ToList();

            // Kiểm tra null trước khi truy cập thuộc tính của ApplicationUser
            ViewBag.CustomerName = order.User?.Fullname;

            ViewBag.CustomerEmail = order.User?.Email;
            ViewBag.ShippingAddress = order.ShippingAddress;
            ViewBag.Notes = order.Notes;

            return View(orderDetails);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound("Không tìm thấy đơn hàng để xóa.");
            }

            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();

            TempData["success"] = "Đơn hàng đã được xóa thành công";
            return RedirectToAction(nameof(Index));
        }


    }
}