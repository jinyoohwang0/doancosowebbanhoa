﻿namespace WebBanHang_2180604857.Models
{
    public class ImageSp
    {
        public int Id { get; set; }

        public string TenFileAnh { get; set; } = null!;

        public short? ViTri { get; set; }

        // Thuộc tính navigation để tạo quan hệ 1-nhiều với bảng Product
        public Product? Product { get; set; } = null!;
    }
}
