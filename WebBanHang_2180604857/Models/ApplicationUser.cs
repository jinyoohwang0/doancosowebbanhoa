﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
namespace WebBanHang_2180604857.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        public string Fullname { get; set; }
    }
}
