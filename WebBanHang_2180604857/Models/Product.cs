﻿

namespace WebBanHang_2180604857.Models
{
    public class Product
    {
        public int Id { get; set; }
        

        public required string Name { get; set; }
   
        public decimal Price { get; set; }
        public required string Description { get; set; }
        public string? ProductImages { get; set; }
        public virtual ICollection<ImageSp> AnhSps { get; set; } = new List<ImageSp>();
        public int CategoryId { get; set; }
        public Category? Category { get; set; }
    }
}
