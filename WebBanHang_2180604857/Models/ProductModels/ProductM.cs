﻿namespace WebBanHang_2180604857.Models.ProductModels
{
    public class ProductM
    {
        public int MaSp { get; set; }
        public required string TenSp { get; set; }
        public int CategoryId { get; set; }
        public string? ProductImages { get; set; }
        public decimal? Price { get; set; }

    }
}
