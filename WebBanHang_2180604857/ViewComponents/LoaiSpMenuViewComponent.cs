﻿using WebBanHang_2180604857.Models;
using Microsoft.AspNetCore.Mvc;
using WebBanHang_2180604857.Repositories;

namespace WebBanHang_2180604857.ViewComponents
{
    public class LoaiSpMenuViewComponent: ViewComponent
    {
        private readonly ICategoryRepository _category;

        public LoaiSpMenuViewComponent(ICategoryRepository categoryRepository)
        {
            _category = categoryRepository;
        }
        public IViewComponentResult Invoke()
        {
            var productcategory = _category.GetAllLoaiSp().OrderBy(X => X.Name);
            return View(productcategory);
        }
    }
}
