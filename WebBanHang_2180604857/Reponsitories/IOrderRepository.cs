﻿using WebBanHang_2180604857.Models;

namespace WebBanHang_2180604857.Repositories
{
    public interface IOrderRepository
    {
        Task<IEnumerable<Order>> GetAllAsync();
        Task<Order> GetByIdAsync(int id);
        Task AddAsync(Order order);
        Task UpdateAsync(Order order);
        Task DeleteAsync(int id);
        Task<List<OrderDetail>> GetOrderDetailByIdAsync(int id);

    }
}
