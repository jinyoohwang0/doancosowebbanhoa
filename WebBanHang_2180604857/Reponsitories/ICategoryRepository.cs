﻿using WebBanHang_2180604857.Models;

namespace WebBanHang_2180604857.Repositories
{
    public interface ICategoryRepository
    {
        Category Add(Category loaiSp);
        Category Update(Category loaiSp);
        Category Delete(String maloaiSp);
        Category GetLoaiSp(String maloaiSp);
        IEnumerable<Category> GetAllLoaiSp();
    }
}
