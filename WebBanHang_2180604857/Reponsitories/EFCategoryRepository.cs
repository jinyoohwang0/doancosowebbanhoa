﻿using WebBanHang_2180604857.DataAccess;
using WebBanHang_2180604857.Models;
using WebBanHang_2180604857.Repositories;

namespace WebBanHang_2180604857.Repositories
{
    public class EFCategoryRepository : ICategoryRepository
    {
        private readonly ApplicationDbContext _context;
        public EFCategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public Category Add(Category loaiSp)
        {
            _context.Categories.Add(loaiSp);
            _context.SaveChanges();
            return loaiSp;
        }

        public Category Delete(string maloaiSp)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> GetAllLoaiSp()
        {
            return _context.Categories;
        }

        public Category GetLoaiSp(string maloaiSp)
        {
            return _context.Categories.Find(maloaiSp);
        }

        public Category Update(Category loaiSp)
        {
            _context.Update(loaiSp);
            _context.SaveChanges();
            return loaiSp;
        }
    }
}
