﻿using WebBanHang_2180604857.Models.ProductModels;
using WebBanHang_2180604857.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebBanHang_2180604857.DataAccess;

namespace WebBanHang_2180604857.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiProductController : ControllerBase
    {
        private readonly ILogger<ApiProductController> _logger;
        private readonly ApplicationDbContext _db;
        public ApiProductController(ApplicationDbContext db, ILogger<ApiProductController> logger)
        {
            _db = db;
            _logger = logger;
        }
        [HttpGet]
        public IEnumerable<ProductM> GetAllProducts()
        {
            var sanPham = (from p in _db.Products
                           select new ProductM
                           {
                               MaSp = p.Id,
                               TenSp = p.Name,
                               CategoryId = p.CategoryId,
                               ProductImages = p.ProductImages,
                               Price = p.Price,
                           }).ToList();
            return sanPham;
        }

        // Tôi sửa lại kiểu trả về thành IEnumerable<ProductM> 
        [HttpGet("{maLoai}")]
        public IEnumerable<ProductM> GetProductsByCategory(int maLoai)
        {
            var sanPham = (from p in _db.Products
                           where p.CategoryId == maLoai
                           select new ProductM
                           {
                               MaSp = p.Id,
                               TenSp = p.Name,
                               CategoryId = p.CategoryId,
                               ProductImages = p.ProductImages,
                               Price = p.Price
                           }).ToList();
            return sanPham;
        }
    }
}


