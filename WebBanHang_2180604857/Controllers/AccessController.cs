﻿using Microsoft.AspNetCore.Mvc;
using WebBanHang_2180604857.Areas.Identity;
using WebBanHang_2180604857.Areas.Identity.Pages.Account;
namespace WebBanHang_2180604857.Controllers
{
    public class AccessController : Controller
    {
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                // Xử lý đăng nhập
            }
            return View(model);
        }
    }

}
