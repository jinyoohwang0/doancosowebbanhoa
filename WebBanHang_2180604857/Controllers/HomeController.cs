﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebBanHang_2180604857.Models;
using WebBanHang_2180604857.DataAccess;
using WebBanHang_2180604857.ViewModels;
using X.PagedList;
using Microsoft.EntityFrameworkCore;

namespace WebBanHang_2180604857.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _db;
        public HomeController(ApplicationDbContext db, ILogger<HomeController> logger)
        {
            _db = db;
            _logger = logger;
        }
        /*Danh muc san pham*/

        public IActionResult Index(int? page)
        {
            int pageSize = 8;
            int pageNumber = page == null || page < 0 ? 1 : page.Value;
            var lstSanPham = _db.Products.AsNoTracking().OrderBy(x => x.Name);
            PagedList<Product> lst = new PagedList<Product>(lstSanPham, pageNumber, pageSize);
            return View(lst);
        }

        /*Phan san pham theo loai*/
        public IActionResult SanPhamTheoLoai(int maloai, int? page) /*Chuyen 2 tham so ma loai va page => phai chuyen 2 tham so vao SP theo loai */
        {
            int pageSize = 8;
            int pageNumber = page == null || page < 0 ? 1 : page.Value;
            var lstSanPham = _db.Products.AsNoTracking().Where(x => x.CategoryId == maloai).OrderBy(x => x.Name);
            PagedList<Product> lst = new PagedList<Product>(lstSanPham, pageNumber, pageSize);
            ViewBag.maloai = maloai;
            return View(lst);
        }
        /*Chi tiet san pham*/
        public IActionResult ChiTietSanPham(int maSp)
        {
            var sanPham = _db.Products.SingleOrDefault(x => x.Id == maSp);
            var anhSanPham = _db.imageSps.Where(x => x.Id == maSp).ToList();
            ViewBag.anhSanPham = anhSanPham;
            return View(sanPham);
        }
        /*Dung viewmodel de tao ra productdetail tao ra xem chi tiet san pham*/
        public IActionResult ProductDetail(int maSp)
        {
            var sanPham = _db.Products.SingleOrDefault(x => x.Id == maSp);
            var anhSanPham = _db.imageSps.Where(x => x.Id == maSp).ToList();
            var homeproductdetailviewmodel = new HomeProductDetailViewModel
            {
                products = sanPham,
                anhSps = anhSanPham
            };
            return View(homeproductdetailviewmodel);
        }
        public IActionResult Search(string searchString, int? page)
        {
            // Kiểm tra xem chuỗi tìm kiếm có rỗng không
            if (string.IsNullOrEmpty(searchString))
            {
                // Nếu chuỗi tìm kiếm rỗng, chuyển hướng người dùng về trang chính
                return RedirectToAction("Index");
            }

            // Tìm kiếm sản phẩm dựa trên chuỗi tìm kiếm
            var products = _db.Products.AsNoTracking().Where(p => p.Name.Contains(searchString)).OrderBy(x => x.Name);

            // Phân trang kết quả tìm kiếm
            int pageSize = 8;
            int pageNumber = page ?? 1;
            var pagedProducts = products.ToPagedList(pageNumber, pageSize);

            // Trả về view chứa kết quả tìm kiếm
            return View("Index", pagedProducts);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
